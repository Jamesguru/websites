# Burmese translation
# Copyright (C) 2021 VideoLAN
# This file is distributed under the same license as the vlc package.
#
# Translators:
# Sithu Aung <sithu.aung015@gmail.com>, 2019-2020
# Yhal Htet Aung <jumoun@gmail.com>, 2013
msgid ""
msgstr ""
"Project-Id-Version: VideoLAN's websites\n"
"Report-Msgid-Bugs-To: vlc-devel@videolan.org\n"
"POT-Creation-Date: 2020-02-05 18:13+0100\n"
"PO-Revision-Date: 2020-07-29 02:35+0200\n"
"Last-Translator: Sithu Aung <sithu.aung015@gmail.com>, 2020\n"
"Language-Team: Burmese (http://www.transifex.com/yaron/vlc-trans/language/"
"my/)\n"
"Language: my\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: include/header.php:289
msgid "a project and a"
msgstr "စီမံကိန်း နှင့် "

#: include/header.php:289
msgid "non-profit organization"
msgstr "အကျိုးအမြတ်မယူသောအဖွဲ့အစည်း"

#: include/header.php:298 include/footer.php:79
msgid "Partners"
msgstr "မိတ်ဖက်များ"

#: include/menus.php:32
msgid "Team &amp; Organization"
msgstr "စည်းမျဉ်း &amp; အဖွဲ့အစည်း"

#: include/menus.php:33
msgid "Consulting Services &amp; Partners"
msgstr "အတိုင်ပင်ခံ ဝန်ဆောင်မှု့ &amp; မိတ်ဖက်များ"

#: include/menus.php:34 include/footer.php:82
msgid "Events"
msgstr "ပွဲများ"

#: include/menus.php:35 include/footer.php:77 include/footer.php:111
msgid "Legal"
msgstr "ဥပဒေ​ရေးရာ"

#: include/menus.php:36 include/footer.php:81
msgid "Press center"
msgstr "သတင်းမီဒီယာ စင်တာ"

#: include/menus.php:37 include/footer.php:78
msgid "Contact us"
msgstr "ကျွန်ုပ် တို့ ကို ဆက်သွယ်ရန်"

#: include/menus.php:43 include/os-specific.php:279
msgid "Download"
msgstr "ဒေါင်းလုပ်"

#: include/menus.php:44 include/footer.php:35
msgid "Features"
msgstr "စွမ်းဆောင်မှုများ"

#: include/menus.php:45 vlc/index.php:57 vlc/index.php:63
msgid "Customize"
msgstr "စိတ်ကြိုက်ပြင်ဆင်နိုင်မှု"

#: include/menus.php:47 include/footer.php:69
msgid "Get Goodies"
msgstr "ကောင်းကျိုးများ ရရှိရန်"

#: include/menus.php:51
msgid "Projects"
msgstr "ပရောဂျက်များ"

#: include/menus.php:71 include/footer.php:41
msgid "All Projects"
msgstr "အားလုံးသော ပရောဂျက်များ"

#: include/menus.php:75 index.php:168
msgid "Contribute"
msgstr "ကူညီမျှဝေ"

#: include/menus.php:77
msgid "Getting started"
msgstr "စတင်ရန်"

#: include/menus.php:78 include/menus.php:96
msgid "Donate"
msgstr "လှူဒါန်းရန်"

#: include/menus.php:79
msgid "Report a bug"
msgstr "အမှား တစ်ခု သတင်းပို့ရန်"

#: include/menus.php:83
msgid "Support"
msgstr "ထောက်ပံ့ကူညီ"

#: include/footer.php:33
msgid "Skins"
msgstr "အပေါ်ခွံများ"

#: include/footer.php:34
msgid "Extensions"
msgstr "ထပ်တိုးများ"

#: include/footer.php:36 vlc/index.php:83
msgid "Screenshots"
msgstr "မျက်နှာပြင်ပုံရိပ်များ"

#: include/footer.php:61
msgid "Community"
msgstr "လူမှုအဖွဲ့စည်း"

#: include/footer.php:64
msgid "Forums"
msgstr "ဆွေးနွေးမှုများ"

#: include/footer.php:65
msgid "Mailing-Lists"
msgstr "စာပို့−စာရင်းများ"

#: include/footer.php:66
msgid "FAQ"
msgstr "အမေးအဖြေ"

#: include/footer.php:67
msgid "Donate money"
msgstr "အလှူငွေ"

#: include/footer.php:68
msgid "Donate time"
msgstr "လှူဒါန်း ကာလ"

#: include/footer.php:75
msgid "Project and Organization"
msgstr "စီမံကိန်း နှင့် အဖွဲ့အစည်း"

#: include/footer.php:76
msgid "Team"
msgstr "အသင်းအဖွဲ့"

#: include/footer.php:80
msgid "Mirrors"
msgstr "အရန်လမ်းကြောင်းများ"

#: include/footer.php:83
msgid "Security center"
msgstr "လုံခြုံရေး စင်တာ"

#: include/footer.php:84
msgid "Get Involved"
msgstr "ပါဝင် ဆောင်ရွက်ရန်"

#: include/footer.php:85
msgid "News"
msgstr "သတင်း"

#: include/os-specific.php:103
msgid "Download VLC"
msgstr "VLC ကို ဒေါင်းလုပ်လုပ်ရန်"

#: include/os-specific.php:109 include/os-specific.php:295 vlc/index.php:168
msgid "Other Systems"
msgstr "အခြားသော စနစ်များအတွက်"

#: include/os-specific.php:260
msgid "downloads so far"
msgstr "ဒေါင်းလုပ် လုပ်ခဲ့ကြသည်"

#: include/os-specific.php:648
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files as well as DVDs, Audio CDs, VCDs, and "
"various streaming protocols."
msgstr ""
"VLC သည် အခမဲ့ နှင့် ပွင့်လင်း ရင်းမြစ် အမျိုးမျိုးသော စနစ်တို့ ကွဲပြားသော မာတီမီဒီယာ ပြစက် နှင့် အမျိုးစုံသော "
"မာတီမီဒီယာ ဖိုင်များ ဒီဗီဒီများ၊ အသံ စီဒီများ၊ ဗီစီဒီများ နှင့် အမျိုးမျိုးသော တိုက်ရိုက်လွှင့်ပြသ သော "
"ပရိုတိုကော တို့ ပြသနိုင်သည့် အခြခံ မူဘောင်လည်းဖြစ်သည်။"

#: include/os-specific.php:652
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files, and various streaming protocols."
msgstr ""
"VLC သည် အခမဲ့ နှင့် ပွင့်လင်း ရင်းမြစ် အမျိုးမျိုးသော စနစ်တို့ ကွဲပြားသော မာတီမီဒီယာ ပြစက် နှင့် အမျိုးစုံသော "
"မာတီမီဒီယာ ဖိုင်များနှင့် တိုက်ရိုက်လွှင့်ထုတ်ပြသည့် အမျိုးမျိုးသော ပရိုတိုကော တို့ ပြသနိုင်သည့် အခြေခံမူဘောင် "
"လည်းဖြစ်သည်။"

#: index.php:4
msgid "VLC: Official site - Free multimedia solutions for all OS!"
msgstr "VLC: တရားဝင် ဆိုက် − အားလုံးသော OS တို့ အတွက် အခမဲ့ မာတီမီဒီယာ ဖြေရှင်းချက် ဖြစ်သည်!"

#: index.php:26
msgid "Other projects from VideoLAN"
msgstr "VideoLan မှ အခြားသော ပရောဂျက်များ"

#: index.php:30
msgid "For Everyone"
msgstr "အားလုံး အတွက်"

#: index.php:40
msgid ""
"VLC is a powerful media player playing most of the media codecs and video "
"formats out there."
msgstr ""
"VLC သည် ထွက်ရှိလျှက်ရှိသည့် မီဒီယာ ကုဒ် နှင့် ဗွီဒီယို အမျိုးအစား အများစု ကို ဖွင့်နိုင်သည့် အစွမ်းထက် သော မီဒီယာ "
"ဖွင့်စက် ဖြစ်သည်။"

#: index.php:53
msgid ""
"VideoLAN Movie Creator is a non-linear editing software for video creation."
msgstr "VideoLAN Movie Creator အစဥ်အလိုက် မဟုတ်သော ဗီဒီယို ဖန်တီး တည်းဖြတ်သည့်ဆော့ဝဲ ဖြစ်သည်။"

#: index.php:62
msgid "For Professionals"
msgstr "သက်မွေးပညာရှင်းများ အတွက်"

#: index.php:72
msgid ""
"DVBlast is a simple and powerful MPEG-2/TS demux and streaming application."
msgstr ""
"DVBlast သည် ရိုးရှင်းပြီး စွမ်းရည်ပြည့်ဝသည့် MPEG-2/TS demux နှင့် ရုပ်သံလွှင့်မှု အပလီကေးရှင်း ဖြစ်သည်။"

#: index.php:82
msgid ""
"multicat is a set of tools designed to easily and efficiently manipulate "
"multicast streams and TS."
msgstr ""
"multicat သည် multicast ရုပ်သံလွှင့်မှု နှင့် TS များ ကို  အလွယ်တကူ ထိရောက်စွာ ကိုင်တွယ်ရန် ဒီဇိုင်းထုတ် "
"ထားသော ကိရိယာများ ဖြစ်သည်။"

#: index.php:95
msgid ""
"x264 is a free application for encoding video streams into the H.264/MPEG-4 "
"AVC format."
msgstr ""
"x264 သည် ဗီဒီယို လွှင့်တင်ခြင်း များကို H.264 / MPEG-4 AVC format သို့ encoding လုပ်ရန် အခမဲ့ "
"အပလီကေးရှင်း ဖြစ်သည်။"

#: index.php:104
msgid "For Developers"
msgstr "ဆော့ဝဲဖန်တီးရေးသားသူများ အတွက်"

#: index.php:140
msgid "View All Projects"
msgstr "စီမံကိန်းများ အားလုံး ကို ကြည့်ရန်"

#: index.php:144
msgid "Help us out!"
msgstr "ကျွန်တော် တို့ကို ကူညီပါ!"

#: index.php:148
msgid "donate"
msgstr "လှူဒါန်းရန်"

#: index.php:156
msgid "VideoLAN is a non-profit organization."
msgstr "VideoLAN သည် အကျိုးအမြတ် မယူသော အဖွဲ့အစည်း တစ်ခု ဖြစ်သည်။"

#: index.php:157
msgid ""
" All our costs are met by donations we receive from our users. If you enjoy "
"using a VideoLAN product, please donate to support us."
msgstr ""
"ကျွန်ုပ်တို့၏ ကုန်ကျစရိတ်များ အားလုံးကို သုံးစွဲသူများ ထံမှ ရရှိသော အလှူငွေများ ဖြင့် ကျခံရပါသည်။ VideoLAN "
"ထုတ်ကုန် ကို သင် ကြိုက်နှစ်သက် ပါက ကျေးဇူးပြု၍ ကျွန်ုပ်တို့ ကို ထောက်ပံ ပါ။"

#: index.php:160 index.php:180 index.php:198
msgid "Learn More"
msgstr "ထပ်မံ လေ့လာရန်"

#: index.php:176
msgid "VideoLAN is open-source software."
msgstr "VideoLAN သည် ပွင့်လင်း−ရင်းမြစ် ဆော့ဝဲ ဖြစ်သည်။"

#: index.php:177
msgid ""
"This means that if you have the skill and the desire to improve one of our "
"products, your contributions are welcome"
msgstr ""
"ဆိုလိုသည်မှာ အကယ်၍ သင့်တွင် ကျွမ်းကျင်မှု ရှိကာ ကျွန်ုပ်တို့၏ ထုတ်ကုန် များထဲမှ တိုးတက်လိုသော တစ်စုံတစ်ရာရှိခဲ့သော် သင်၏ "
"ပူးပေါင်းပါဝင်မှု ကို ကြိုဆိုပါသည်။"

#: index.php:187
msgid "Spread the Word"
msgstr "ပန်းသတင်း လေညှင်း ဆောင် လူသတင်း လူချင်း ဆောင်"

#: index.php:195
msgid ""
"We feel that VideoLAN has the best video software available at the best "
"price: free. If you agree please help spread the word about our software."
msgstr ""
"VideoLAN သည် အခမဲ့ ဆိုသော အကောင်းဆုံး စျေးနှုန်းဖြင့် အကောင်းဆုံး ဗီဒီယို ဆော့ဝဲ ကို ရရှိနေသည် ဟု "
"ကျွန်တော်တို့ ခံစားမိသည်။ အကယ်၍ သင်သည်လဲ ထိုသို့ ခံယူချက်ကို ထောက်ခံပါက ကျွန်တော်တို့ ဆော့ဝဲ အကြောင်း ကို "
"ကျေးဇူးပြု၍ ဖြန်ဝေ ပေးပါ။"

#: index.php:215
msgid "News &amp; Updates"
msgstr "သတင်း &amp; အဆင့်မြင့်ပြင်ဆင်မှုများ"

#: index.php:218
msgid "More News"
msgstr "နောက်ထပ် သတင်းများ"

#: index.php:222
msgid "Development Blogs"
msgstr "တည်ဆောက်ရေးသားခြင်း ဘလော့ဂ်များ"

#: index.php:251
msgid "Social media"
msgstr "လူမှု မီဒီယာ"

#: vlc/index.php:3
msgid "Official download of VLC media player, the best Open Source player"
msgstr ""
"အကောင်းဆုံးသော ပွင့်လင်း ရင်းမြစ် ပြစက် ဖြစ်သော VLC မီဒီယာ ပြစက် ၏ တရားဝင် ဒေါင်းလုပ်လုပ်ရန် နေရာ။"

#: vlc/index.php:21
msgid "Get VLC for"
msgstr "VLC ကို ရယူခြင်း အတွက်"

#: vlc/index.php:29 vlc/index.php:32
msgid "Simple, fast and powerful"
msgstr "ရိုးရှင်း၊ မြန်ဆန် ကာ စွမ်းဆောင်ရည် ပြည့်ဝသည်"

#: vlc/index.php:35
msgid "Plays everything"
msgstr "အရာအားလုံး ဖွင့်လှစ်ပြသ နိုင်သည်"

#: vlc/index.php:35
msgid "Files, Discs, Webcams, Devices and Streams."
msgstr "ဖိုင်များ၊ အခွေများ၊ ဝက်ဘ်ကင်မရာများ၊ ကိရိယာများ နှင့် တိုက်ရိုက်ပြသခြင်းများ "

#: vlc/index.php:38
msgid "Plays most codecs with no codec packs needed"
msgstr "ကုဒ် ဖိုင်များ ထပ်မံမလိုအပ်ပဲ ကုဒ်အများစု ကို ဖွင့်လှစ်ပြသနိုင်"

#: vlc/index.php:41
msgid "Runs on all platforms"
msgstr "အားလုံးသော စနစ် များ တွင် အလုပ်လုပ်နိုင်"

#: vlc/index.php:44
msgid "Completely Free"
msgstr "အလုံးစုံ အခမဲ့"

#: vlc/index.php:44
msgid "no spyware, no ads and no user tracking."
msgstr "စပိုင်ဝဲ မပါ၊ ကြော်ြငာ မပါ၊ အသုံးပြုသူ နောက်ယောင်ခံမှု မပါ"

#: vlc/index.php:47
msgid "learn more"
msgstr "ထပ်မံ လေ့လာရန်"

#: vlc/index.php:66
msgid "Add"
msgstr "ထည့်သွင်းရန်"

#: vlc/index.php:66
msgid "skins"
msgstr "အခွံများ"

#: vlc/index.php:69
msgid "Create skins with"
msgstr "အခွံများ ပြုလုပ် ရန်"

#: vlc/index.php:69
msgid "VLC skin editor"
msgstr "VLC အခွံ editor  ဖြင့်"

#: vlc/index.php:72
msgid "Install"
msgstr "ထည့်သွင်းရန်"

#: vlc/index.php:72
msgid "extensions"
msgstr "ထပ်တိုး စနစ်များ"

#: vlc/index.php:126
msgid "View all screenshots"
msgstr "မျက်နှာပြင်ပုံရိပ်များ အားလုံး ကြည့်ရန်"

#: vlc/index.php:135
msgid "Official Downloads of VLC media player"
msgstr "VLC မီဒီယာ ပြစက် ၏ တရားဝင် ဒေါင်းလုပ်လုပ်ရန်"

#: vlc/index.php:146
msgid "Sources"
msgstr "ရင်းမြစ်များ"

#: vlc/index.php:147
msgid "You can also directly get the"
msgstr "သင် သည် လည်း တိုက်ရိုက် ရရှိနိုင် "

#: vlc/index.php:148
msgid "source code"
msgstr "မူရင်း ကုဒ်"

#~ msgid "A project and a"
#~ msgstr "ပရောဂျက်နှင့်"

#~ msgid ""
#~ "composed of volunteers, developing and promoting free, open-source "
#~ "multimedia solutions."
#~ msgstr ""
#~ "တွင်စေတနာ့ဝန်ထမ်းများ၊ ထုတ်လုပ်ပြီးနှင့်အခမဲ့၊ ရင်းမြစ်ပွင့်၊ မာလ်တီမီဒီယာအဖြေများအထောက်အကူပြုခြင်း။"

#~ msgid "why?"
#~ msgstr "ဘာကြောင့်?"

#~ msgid "Home"
#~ msgstr "ပင်မ"

#~ msgid "Support center"
#~ msgstr "ထောက်ပံ့စင်တာ"

#~ msgid "Dev' Zone"
#~ msgstr "ပရိုဂရမ်ရေးသားသူဇုန်"

#~ msgid "Simple, fast and powerful media player."
#~ msgstr "ရိုးရှင်း၊ မြန်ဆန်ပြီးအားကောင်းသောမီဒီယာဖွင့်စက်။"

#~ msgid "Plays everything: Files, Discs, Webcams, Devices and Streams."
#~ msgstr ""
#~ "အားလံုးကိုဖွင့်နို်င်: ဖိုင်များ၊ အချပ်များ၊ ဝဘ်ကမ်များ၊ ပစ္စည်းများနှင့်သီချင်းစီးကြောင်းများ။"

#~ msgid "Plays most codecs with no codec packs needed:"
#~ msgstr "အသံချုံချဲထုပ်များမလိုဘဲအသံချုံချဲအများစုကိုဖွင့်နို်င်:"

#~ msgid "Runs on all platforms:"
#~ msgstr "စက်လည်ပတ်စနစ်များအားလံုးတွင်လုပ်ဆောင်နိုင်:"

#~ msgid "Completely Free, no spyware, no ads and no user tracking."
#~ msgstr "လံုးဝအခမဲ့၊ ၀ သူလျှိဝဲ၊ ၀ ကြော်ငြာဝဲများနှင့်အသံုးပြုသူခြေရာကောက်ခြင်းများမရှိ။"

#~ msgid "Can do media conversion and streaming."
#~ msgstr "ဗီဒီယာပြောင်းလဲခြင်းနှင့်သီချင်းစီးကြောင်းများထုတ်လွှင့်ခြင်းကိုလုပ်ဆောင်နို်င်။"

#~ msgid "Discover all features"
#~ msgstr "အင်္ဂါရပ်များအားလံုးရှာဖွေ"

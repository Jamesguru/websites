# Indonesian translation
# Copyright (C) 2021 VideoLAN
# This file is distributed under the same license as the vlc package.
#
# Translators:
# Anthony Santana, 2015
# Aulia Firdaus Simbolon <dausbajing@hotmail.co.id>, 2013
# Daffa Ridho Fadhlika Muhammad <daffa.otaku@gmail.com>, 2013
# duniadanu <duniadanu@gmail.com>, 2014
# Luqman Hakim <ovdl@rocketmail.com>, 2015,2019
# Tony Bennjamin <anthonioustony@hotmail.com>, 2014
# zk <zamani.karmana@gmail.com>, 2015
msgid ""
msgstr ""
"Project-Id-Version: VideoLAN's websites\n"
"Report-Msgid-Bugs-To: vlc-devel@videolan.org\n"
"POT-Creation-Date: 2020-02-05 18:13+0100\n"
"PO-Revision-Date: 2019-04-20 15:34+0200\n"
"Last-Translator: Luqman Hakim <ovdl@rocketmail.com>, 2019\n"
"Language-Team: Indonesian (http://www.transifex.com/yaron/vlc-trans/language/"
"id/)\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: include/header.php:289
msgid "a project and a"
msgstr "sebuah proyek dan sebuah"

#: include/header.php:289
msgid "non-profit organization"
msgstr "organisasi nirlaba"

#: include/header.php:298 include/footer.php:79
msgid "Partners"
msgstr "Rekanan"

#: include/menus.php:32
msgid "Team &amp; Organization"
msgstr "Tim &amp Organisasi"

#: include/menus.php:33
msgid "Consulting Services &amp; Partners"
msgstr "Layanan Konsultasi &amp Rekanan"

#: include/menus.php:34 include/footer.php:82
msgid "Events"
msgstr "Acara-acara"

#: include/menus.php:35 include/footer.php:77 include/footer.php:111
msgid "Legal"
msgstr "Legal"

#: include/menus.php:36 include/footer.php:81
msgid "Press center"
msgstr "Pusat pers"

#: include/menus.php:37 include/footer.php:78
msgid "Contact us"
msgstr "Hubungi kami"

#: include/menus.php:43 include/os-specific.php:279
msgid "Download"
msgstr "Unduh"

#: include/menus.php:44 include/footer.php:35
msgid "Features"
msgstr "Fitur"

#: include/menus.php:45 vlc/index.php:57 vlc/index.php:63
msgid "Customize"
msgstr "Kustomisasi"

#: include/menus.php:47 include/footer.php:69
msgid "Get Goodies"
msgstr "Dapatkan Cendera Mata"

#: include/menus.php:51
msgid "Projects"
msgstr "Proyek"

#: include/menus.php:71 include/footer.php:41
msgid "All Projects"
msgstr "Semua Proyek"

#: include/menus.php:75 index.php:168
msgid "Contribute"
msgstr "Kontribusi"

#: include/menus.php:77
msgid "Getting started"
msgstr "Memulai"

#: include/menus.php:78 include/menus.php:96
msgid "Donate"
msgstr "Sumbangan"

#: include/menus.php:79
msgid "Report a bug"
msgstr "Laporkan kutu"

#: include/menus.php:83
msgid "Support"
msgstr "Dukungan"

#: include/footer.php:33
msgid "Skins"
msgstr "Pemalut"

#: include/footer.php:34
msgid "Extensions"
msgstr "Ekstensi"

#: include/footer.php:36 vlc/index.php:83
msgid "Screenshots"
msgstr "Cuplikan Layar"

#: include/footer.php:61
msgid "Community"
msgstr "Komunitas"

#: include/footer.php:64
msgid "Forums"
msgstr "Forum"

#: include/footer.php:65
msgid "Mailing-Lists"
msgstr "Milis"

#: include/footer.php:66
msgid "FAQ"
msgstr "FAQ"

#: include/footer.php:67
msgid "Donate money"
msgstr "Sumbangan uang"

#: include/footer.php:68
msgid "Donate time"
msgstr "Sumbangan waktu"

#: include/footer.php:75
msgid "Project and Organization"
msgstr "Proyek dan Organisasi"

#: include/footer.php:76
msgid "Team"
msgstr "Tim"

#: include/footer.php:80
msgid "Mirrors"
msgstr "Tautan Lain"

#: include/footer.php:83
msgid "Security center"
msgstr "Pusat keamanan"

#: include/footer.php:84
msgid "Get Involved"
msgstr "Ikut Terlibat"

#: include/footer.php:85
msgid "News"
msgstr "Berita"

#: include/os-specific.php:103
msgid "Download VLC"
msgstr "Unduh VLC"

#: include/os-specific.php:109 include/os-specific.php:295 vlc/index.php:168
msgid "Other Systems"
msgstr "Sistem Lainnya"

#: include/os-specific.php:260
msgid "downloads so far"
msgstr "unduhan sejauh ini"

#: include/os-specific.php:648
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files as well as DVDs, Audio CDs, VCDs, and "
"various streaming protocols."
msgstr ""
"VLC adalah kerangka kerja dan pemutar multimedia lintas-platform gratis dan "
"bersumber terbuka yang dapat memutar sebagian besar berkas multimedia serta "
"DVD, CD Audio, VCD, dan berbagai protokol pengaliran."

#: include/os-specific.php:652
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files, and various streaming protocols."
msgstr ""
"VLC adalah kerangka kerja dan pemutar multimedia lintas-platform gratis dan "
"bersumber terbuka yang dapat memutar sebagian besar berkas multimedia, dan "
"berbagai protokol streaming."

#: index.php:4
msgid "VLC: Official site - Free multimedia solutions for all OS!"
msgstr "VLC: Situs resmi - solusi multimedia gratis untuk semua OS!"

#: index.php:26
msgid "Other projects from VideoLAN"
msgstr "Proyek lain dari VideoLAN"

#: index.php:30
msgid "For Everyone"
msgstr "Untuk Semua Orang"

#: index.php:40
msgid ""
"VLC is a powerful media player playing most of the media codecs and video "
"formats out there."
msgstr ""
"VLC adalah pemutar media luar biasa yang dapat memutar hampir semua format "
"video dan codec."

#: index.php:53
msgid ""
"VideoLAN Movie Creator is a non-linear editing software for video creation."
msgstr ""
"VideoLAN Movie Creator adalah perangkat lunak penyunting non-linear untuk "
"pembuatan video."

#: index.php:62
msgid "For Professionals"
msgstr "Untuk Profesional"

#: index.php:72
msgid ""
"DVBlast is a simple and powerful MPEG-2/TS demux and streaming application."
msgstr ""
"DVBlast adalah demultiplekser dan aplikasi pengalir MPEG-2/TS yang sederhana "
"dan luar biasa."

#: index.php:82
msgid ""
"multicat is a set of tools designed to easily and efficiently manipulate "
"multicast streams and TS."
msgstr ""
"multicat adalah kumpulan perkakas yang dirancang untuk dengan mudah dan "
"efisien memanipulasi aliran multicast dan TS."

#: index.php:95
msgid ""
"x264 is a free application for encoding video streams into the H.264/MPEG-4 "
"AVC format."
msgstr ""
"x264 adalah aplikasi gratis untuk mengubah aliran video menjadi format H.264/"
"MPEG-4 AVC."

#: index.php:104
msgid "For Developers"
msgstr "Untuk Pengembang"

#: index.php:140
msgid "View All Projects"
msgstr "Lihat Semua Proyek"

#: index.php:144
msgid "Help us out!"
msgstr "Bantu kami!"

#: index.php:148
msgid "donate"
msgstr "donasi"

#: index.php:156
msgid "VideoLAN is a non-profit organization."
msgstr "VideoLAN adalah organisasi nirlaba."

#: index.php:157
msgid ""
" All our costs are met by donations we receive from our users. If you enjoy "
"using a VideoLAN product, please donate to support us."
msgstr ""
"Semua pendapatan kami didapat dari donasi yang kami terima dari pengguna. "
"Jika Anda suka menggunakan produk VideoLAN, sebaiknya donasikan sejumlah "
"uang untuk mendukung kami."

#: index.php:160 index.php:180 index.php:198
msgid "Learn More"
msgstr "Pelajari Lebih Lanjut"

#: index.php:176
msgid "VideoLAN is open-source software."
msgstr "VideoLAN adalah perangkat lunak sumber terbuka."

#: index.php:177
msgid ""
"This means that if you have the skill and the desire to improve one of our "
"products, your contributions are welcome"
msgstr ""
"Artinya adalah jika Anda memiliki keahlian dan keinginan untuk meningkatkan "
"kualitas salah satu produk kami, kontribusi Anda sangat diterima"

#: index.php:187
msgid "Spread the Word"
msgstr "Sebarkan Berita"

#: index.php:195
msgid ""
"We feel that VideoLAN has the best video software available at the best "
"price: free. If you agree please help spread the word about our software."
msgstr ""
"Kami rasa VideoLAN memiliki perangkat lunak video yang tersedia dengan harga "
"terbaik: gratis. Jika Anda setuju tolong sebarkan berita tentang perangkat "
"lunak kami."

#: index.php:215
msgid "News &amp; Updates"
msgstr "Berita &amp; Pemutakhiran"

#: index.php:218
msgid "More News"
msgstr "Berita Lanjutan"

#: index.php:222
msgid "Development Blogs"
msgstr "Blog Pengembangan"

#: index.php:251
msgid "Social media"
msgstr "Media Sosial"

#: vlc/index.php:3
msgid "Official download of VLC media player, the best Open Source player"
msgstr "Unduhan resmi pemutar media VLC, pemutar Sumber Terbuka terbaik"

#: vlc/index.php:21
msgid "Get VLC for"
msgstr "Dapatkan VLC untuk"

#: vlc/index.php:29 vlc/index.php:32
msgid "Simple, fast and powerful"
msgstr "Sederhana, cepat dan bertenaga"

#: vlc/index.php:35
msgid "Plays everything"
msgstr "Memutar semuanya"

#: vlc/index.php:35
msgid "Files, Discs, Webcams, Devices and Streams."
msgstr "Berkas, Disk, Webcam, Perangkat dan Stream."

#: vlc/index.php:38
msgid "Plays most codecs with no codec packs needed"
msgstr "Memutar sebagian besar kodek tanpa membutuhkan paket kodek"

#: vlc/index.php:41
msgid "Runs on all platforms"
msgstr "Berjalan pada semua platform"

#: vlc/index.php:44
msgid "Completely Free"
msgstr "Gratis Sepenuhnya"

#: vlc/index.php:44
msgid "no spyware, no ads and no user tracking."
msgstr "tidak ada spyware, tidak ada iklan dan tidak ada pelacakan pengguna."

#: vlc/index.php:47
msgid "learn more"
msgstr "pelajari lebih lanjut"

#: vlc/index.php:66
msgid "Add"
msgstr "Tambahkan"

#: vlc/index.php:66
msgid "skins"
msgstr "skin"

#: vlc/index.php:69
msgid "Create skins with"
msgstr "Buat skin dengan"

#: vlc/index.php:69
msgid "VLC skin editor"
msgstr "VLC skin editor"

#: vlc/index.php:72
msgid "Install"
msgstr "Pasang"

#: vlc/index.php:72
msgid "extensions"
msgstr "ekstensi"

#: vlc/index.php:126
msgid "View all screenshots"
msgstr "Lihat semua cuplikan layar"

#: vlc/index.php:135
msgid "Official Downloads of VLC media player"
msgstr "Unduhan Resmi untuk VLC media player"

#: vlc/index.php:146
msgid "Sources"
msgstr "Sumber"

#: vlc/index.php:147
msgid "You can also directly get the"
msgstr "Anda juga dapat secara langsung mendapatkan"

#: vlc/index.php:148
msgid "source code"
msgstr "kode sumber"

#~ msgid "A project and a"
#~ msgstr "Sebuah proyek dan "

#~ msgid ""
#~ "composed of volunteers, developing and promoting free, open-source "
#~ "multimedia solutions."
#~ msgstr ""
#~ "terdiri dari para relawan, yang mengembangkan dan mempromosikan solusi "
#~ "multimedia sumber terbuka gratis."

#~ msgid "why?"
#~ msgstr "mengapa?"

#~ msgid "Home"
#~ msgstr "Beranda"

#~ msgid "Support center"
#~ msgstr "Pusat dukungan"

#~ msgid "Dev' Zone"
#~ msgstr "Zona Pengembang"

#~ msgid "Simple, fast and powerful media player."
#~ msgstr "Pemutar media yang sederhana, cepat dan luar biasa."

#~ msgid "Plays everything: Files, Discs, Webcams, Devices and Streams."
#~ msgstr "Memutar semuanya: Berkas, Diska, Webkam, Perangkat dan Stream."

#~ msgid "Plays most codecs with no codec packs needed:"
#~ msgstr "Memainkan hampir semua kodek tanpa perlu memasang kodek lagi:"

#~ msgid "Runs on all platforms:"
#~ msgstr "Berjalan pada semua platform:"

#~ msgid "Completely Free, no spyware, no ads and no user tracking."
#~ msgstr ""
#~ "Gratis Sepenuhnya, tanpa perangkat mata-mata, tanpa iklan dan tanpa "
#~ "pelacakan aktivitas pengguna."

#~ msgid "Can do media conversion and streaming."
#~ msgstr "Dapat melakukan konversi dan pengaliran media."

#~ msgid "Discover all features"
#~ msgstr "Temukan semua fitur"

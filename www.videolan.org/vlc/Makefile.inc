PHP_IGNORES = download-skins2 download-skins2-go skins_old
SUBDIR = contest releases stats

PHP_FILES := index features libvlc screenshots skineditor skineditor_update \
             skins streaming privacy \
             download-android download-archlinux download-beos download-crux download-chromeos download-debian download-eyetv download-familiar download-fedora download-freebsd download-gentoo download-ios download-appletv download-winrt download-linupy download-macosx download-redhat download-skins2-go download-skins download-slackware download-sources download-suse download-ubuntu download-wince download-windows download-windowsphone

PHP_FILES += $(addprefix index.,$(shell find $(ROOT)/locale -maxdepth 1 -mindepth 1 -type d | sed "s,$(ROOT)/locale/,,"))
PHP_FILES += $(addprefix download-windows.,$(shell find $(ROOT)/locale -maxdepth 1 -mindepth 1 -type d | sed "s,$(ROOT)/locale/,,"))
PHP_FILES += $(addprefix download-macosx.,$(shell find $(ROOT)/locale -maxdepth 1 -mindepth 1 -type d | sed "s,$(ROOT)/locale/,,"))

download-windows.%.php: download-windows.php $(ROOT)/locale/%/LC_MESSAGES/website.po
	rm -f ./$@ && sed -e "s/language = \"\"/language = \"$*\"/" download-windows.php > ./$@;
	msgfmt -c -v -o $(ROOT)/locale/$*/LC_MESSAGES/website.mo $(ROOT)/locale/$*/LC_MESSAGES/website.po

download-macosx.%.php: download-macosx.php $(ROOT)/locale/%/LC_MESSAGES/website.po
	rm -f ./$@ && sed -e "s/language = \"\"/language = \"$*\"/" download-macosx.php > ./$@;
	msgfmt -c -v -o $(ROOT)/locale/$*/LC_MESSAGES/website.mo $(ROOT)/locale/$*/LC_MESSAGES/website.po

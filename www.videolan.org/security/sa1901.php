<?php
   $title = "VideoLAN Security Advisory 1901";
   $lang = "en";
   $menu = array( "vlc" );
   $body_color = "red";
   require($_SERVER["DOCUMENT_ROOT"]."/include/header.php");
?>


<div id="fullwidth">

<h1>Security Advisory 1901</h1>
<pre>
Summary           : Read buffer overflow &amp; double free
Date              : June 2019
Affected versions : VLC media player 3.0.6 and earlier
ID                : VideoLAN-SA-1901
CVE reference     : CVE-2019-5439, CVE-2019-12874
</pre>

<h2>Details</h2>
<p>A remote user can create some specially crafted avi or mkv files that, when loaded by the target user, will trigger a
heap buffer overflow (read) in ReadFrame (demux/avi/avi.c), or a
double free in zlib_decompress_extra() (demux/mkv/utils.cpp) respectively</p>

<h2>Impact</h2>
<p>If successful, a malicious third party could trigger either a crash of VLC or an arbitratry code execution with the privileges of the target user.</p>

<h2>Threat mitigation</h2>
<p>Exploitation of those issues requires the user to explicitly open a specially crafted file or stream.</p>

<h2>Workarounds</h2>
<p>The user should refrain from opening files from untrusted third parties
or accessing untrusted remote sites (or disable the VLC browser plugins),
until the patch is applied.
</p>

<h2>Solution</h2>
<p>VLC media player <b>3.0.7</b> addresses the issues.
This release also fixes an important security issue that could lead to code execution when playing an AAC file.
</p>

<h2>Credits</h2>
<p>The MKV double free vulnerability was reported by Symeon Paraschoudis from Pen Test Partners</p>

<h2>References</h2>
<dl>
<dt>The VideoLAN project</dt>
<dd><a href="//www.videolan.org/">http://www.videolan.org/</a>
</dd>
<dt>VLC official GIT repository</dt>
<dd><a href="http://git.videolan.org/?p=vlc/vlc-3.0.git">http://git.videolan.org/?p=vlc.git</a>
</dd>
</dl>

</div>

<?php footer('$Id$'); ?>
